# RCE R Tools
# Author: Gregor Thomas gthomas@projectevident.org
# Project Evident
# May 10, 2019


##### Randomization

## Let's get some hypothetical data to do a simple random sample

## setting a random seed let's us draw the same random numbers again if we need to
set.seed(48) 
n = 100
id_data = data.frame(
  id = sample(1001:1500, size = n),
  site = sample(c("A", "B", "C"), size = n, replace = TRUE)
)

## We will add a column of random numbers,
## sort by the random numbers 
## and assign the first half to Control 
## and the second half to treatment
library(dplyr)
id_data = id_data %>%
  mutate(rand = runif(n())) %>%
  arrange(rand) %>%
  mutate(group = ifelse(row_number() <= n() / 2, "control", "treatment"))

## and write the result to a file with today's date
## I like to use tab separated  files instead of comma separative files
## theTab can be written as "\t"
write.table(id_data,
            file = paste0("rce/groups-", Sys.Date(), ".tsv"),
            sep = "\t")

## Let's look at how the groups worked out by site
## The bigger the population, the less this matters
table(id_data$site, id_data$group)
plot(table(id_data$site, id_data$group))


## If instead we wanted to do a Stratified Random Sample, thanks to
## dplyr's ease of doing things "by group", we just add a `group_by`
## call and otherwise use the same code

strat_data = id_data %>%
  group_by(site) %>%
  mutate(rand = runif(n())) %>%
  arrange(rand) %>%
  mutate(group = ifelse(row_number() <= n() / 2, "control", "treatment"))

write.table(strat_data,
            file = paste0("rce/groups-strat-", Sys.Date(), ".tsv"),
            sep = "\t")

plot(table(strat_data$site, strat_data$group))

## If we had any reason to believe the effectiveness of the experiment
## will depend on the site, using the stratified random sample might be 
## better.

## However, most of the time a stratified sample is unnecessary. Check out 
## features of concern the sample you get using the table function,
## if things seem imbalanced, just re-randomize (using a different seed
## if you are using set.seed)


#####  Power Calculations

## Necessary for planning an experiment.

## We will use the handy power.prop.test function for power calcs
## since we are planning on using prop.test for analysis

?power.prop.test

## It assumes we have the same number of observations in each group, called n
## It expects one of its arguments to be missing, and it will solve for the missing one
power.prop.test(
  n = 360 * 3,
  p1 = 0.9,
  power = 0.8,
  sig.level = 0.20
)

power.prop.test(
  n = 260,
  p1 = 0.05,
  power = 0.8,
  sig.level = 0.05
)

## play around with inputs, adjusting sample size (e.g., how many months to
## run an experiment for can increase sample size), significance and power level
## until you are satisfied with the effect size.
## install and use the pwr package for more full-featured power calc functions


##### Analyzing results

## We will use prop.test

?prop.test

prop.test(x = c(300, 320), n = c(350, 350), conf.level = 0.95)
## prop.test gives a confidence interval for the 1st minus the 2nd
## look at the p-value (is it less than 1 - the confidence level?)
## look at the confidence interval (does it include 0?)

## If results are statistically significant, then you will get both
## of the below:
## 1) The p-value will be *less than* significance level (= 1 - confidence level) 
## 2) The confidence interval *will not include 0*
## If your results are not significant, then you will get neither of the above.

## Not significant results:
##   "We cannot be [confidence level] sure that the difference is not due to chance.

## Significant results:
##   "We are [confidence level] sure that there is a difference between treatment and control.
##   or "The difference between treatment and control is significant at the [significance level] level.

## Use t.test() for continuous outcomes


##### Bayesian Version of a T-Test
library(BEST)
y1 <- c(5.77, 5.33, 4.59, 4.33, 3.66, 4.48)
y2 <- c(3.88, 3.55, 3.29, 2.59, 2.33, 3.59)

# Run an analysis, takes up to 1 min.
BESTout <- BESTmcmc(y1, y2)

# Look at the result:
BESTout
summary(BESTout)
plot(BESTout)


## Power analysis depends on your model and methods. THe BEST package offers 
## functions and examples for doing Power Analysis.